#!/bin/sh

mkdir transport
mkdir signing

chmod 755 transport
chmod 755 signing

echo $1
echo $2

echo $date

openssl req -new -newkey rsa:2048  -sha256 -nodes -out transport/transport_$1.csr -keyout transport/transport_$1.key -subj "/C=GB/O=OpenBanking/OU=$2/CN=$1"

openssl req -new -newkey rsa:2048  -sha256 -nodes -out signing/signing_$1.csr -keyout signing/signing_$1.key -subj "/C=GB/O=OpenBanking/OU=$2/CN=$1"

